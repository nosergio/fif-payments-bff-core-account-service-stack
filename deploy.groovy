def stacks = [
  QA: 'payments-wallet/stacks/qa/add-cards-qa',
  PROD: 'payments-wallet/stacks/prod/add-cards-prod']
pipeline {
  agent {
    node {
      label 'azure-slave'
    }
  }
  environment{
    GIT_DEV_LAST_TAG = "latest"
    GIT_QA_LAST_TAG = "latest-qa"
    GIT_PROD_LAST_TAG = "latest-prod"
    SLACK_CHANNEL = '#payments-cc3-builds'
  }
  stages {
    stage('Promote to QA'){
      when {
        expression { 
            params.ENVIRONMENT == 'QA'
        }
      }
      steps {
        sh "docker pull registry.fif.tech/${params.SERVICE_NAME}:${GIT_DEV_LAST_TAG}"
        sh "docker tag registry.fif.tech/${params.SERVICE_NAME}:${GIT_DEV_LAST_TAG} registry.fif.tech/${params.SERVICE_NAME}:${GIT_QA_LAST_TAG}"
        sh "docker push registry.fif.tech/${params.SERVICE_NAME}:${GIT_QA_LAST_TAG}"
        script {
          try {
            sh('docker rmi $(docker images -f "dangling=true" -q) --force')
          } catch (Exception ex){
            sh('echo "No hay Imagenes que borrar"')
          }
        }
      }
    }
    stage('Promote to PROD'){
      when {
        expression { 
            params.ENVIRONMENT == 'PROD'
        }
      }
      steps {
        sh "docker pull registry.fif.tech/${params.SERVICE_NAME}:${GIT_QA_LAST_TAG}"
        sh "docker tag registry.fif.tech/${params.SERVICE_NAME}:${GIT_QA_LAST_TAG} registry.fif.tech/${params.SERVICE_NAME}:${GIT_PROD_LAST_TAG}"
        sh "docker push registry.fif.tech/${params.SERVICE_NAME}:${GIT_PROD_LAST_TAG}"
       script {
         try {
           sh('docker rmi $(docker images -f "dangling=true" -q) --force')
         } catch (Exception ex){
           sh('echo "No hay Imagenes que borrar"')
         }
       }
      }
    }
    stage ('Deploy Stack QA') {
      when {
        expression { 
            params.ENVIRONMENT == 'QA'
        }
      }
      steps {
        build job: stacks[params.ENVIRONMENT], wait: false
      }
    }
    stage ('Deploy Stack PROD') {
      when {
        expression { 
            params.ENVIRONMENT == 'PROD'
        }
      }
      steps {
        build job: stacks[params.ENVIRONMENT], wait: false
      }
    }
  }
  post {
    cleanup {
      echo 'Clean up workspace'
      cleanWs()
      deleteDir()
    }
    success {
      echo 'Execution Success'
      bitbucketStatusNotify ( buildState: 'SUCCESSFUL' )
      slackSend channel: env.SLACK_CHANNEL,
                color: 'good',
                message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})"
    }
    failure {
      echo 'Execution Failed'
      bitbucketStatusNotify ( buildState: 'FAILED' )
      slackSend channel: env.SLACK_CHANNEL,
                color: 'bad',
                message: "FAILURE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})"
    }
  }
}

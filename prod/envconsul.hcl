vault {
    address = "https://vault.fif.tech:8200"
    unwrap_token = false
    renew_token = true
    grace = "120h"

    ssl {
        enabled = true
        verify = true
    }
}
 
upcase = false
log_level = "err"
 
exec {
    command = "/bin/sh"
}

secret {
    format = "{{ key }}"
    no_prefix = true
    path = "kv/clusters/fif-payments-wallet-prod-cluster/shared/manual/pismo/pismo-org-2"
}

secret {
    format = "{{ key }}"
    no_prefix = true
    path = "kv/clusters/fif-payments-wallet-prod-cluster/priv/tools/rabbitmq/users/core-operations"
}

secret {
    format = "{{ key }}"
    no_prefix = true
    path = "kv/clusters/fif-payments-wallet-prod-cluster/priv/tools/redis"
}

secret {
    format = "{{ key }}"
    no_prefix = true
    path = "kv/clusters/fif-payments-wallet-prod-cluster/shared/manual/pci-auth"
}